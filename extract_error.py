#!/bin/python

import re

def extract_kvm():
    _line_re = re.compile(r'^(?:\[\s*(?P<timestamp>\d+\.\d+)\]\s+)?(?P<message>.*)$')

    with open("kvm.log") as file:
        for line in file:
            res = _line_re.search(line)
            if res.group('message').startswith("kvm"):
                print (res[0])

def extract_libvirt():
    with open("libvirt.log") as file:
        for line in file:
            if "error" in line:
                print (line)

def extract_qemu():
    with open("qemu.log") as file:
        for line in file:
            if "failed" in line:
                print(line)

if __name__ == "__main__":
    extract_kvm()
    extract_libvirt()
    extract_qemu()
